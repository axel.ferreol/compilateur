open Batteries
open Cfg
open Elang_run
open Prog
open Utils
open Report
open Cfg_print
open Options

(* [simple_eval_eexpr e] evaluates an expression [e] with no variables. Raises
   an exception if the expression contains variables. *)
let rec simple_eval_eexpr (e: expr) : int =
  match e with
  | Ebinop (bin_op,expr1,expr2) -> (eval_binop bin_op) (simple_eval_eexpr expr1) (simple_eval_eexpr expr2)
  | Eunop (un_op,expr0) -> (eval_unop un_op) (simple_eval_eexpr expr0)
  | Eint(i) -> i
  | Evar(v) -> failwith("The expression contains variables!")
  


(* If an expression contains variables, we cannot simply evaluate it. *)

(* [has_vars e] indicates whether [e] contains variables. *)
let rec has_vars (e: expr) =
   match e with
    | Ebinop(_,expr1,expr2) -> (has_vars expr1) || (has_vars expr2)
    | Eunop(_,expr0) -> has_vars expr0
    | Eint(_) -> false
    | Evar(_) -> true

let const_prop_binop b e1 e2 =
  let e = Ebinop (b, e1, e2) in

  match has_vars e1, has_vars e2 with
  |true,true -> e
  |true,false -> (match b,simple_eval_eexpr e2 with
                  |Eadd,0 -> e1
                  |Esub,0 -> e1
                  |Emul,0 -> Eint(0)
                  |Emul,1 -> e1
                  |_,_ -> e)
  |false,true -> (match b,simple_eval_eexpr e1 with
                  |Eadd,0 -> e2
                  |Esub,0 -> Eunop(Eneg, e2)
                  |Emul,0 -> Eint(0)
                  |Emul,1 -> e2
                  |_,_ -> e)
  |false,false -> Eint(simple_eval_eexpr e)


let const_prop_unop u e =
  let e = Eunop (u, e) in
  if has_vars e
  then e
  else Eint (simple_eval_eexpr e)


let rec const_prop_expr (e: expr) : expr =
   match e with
   | Ebinop(bin_op,expr1,expr2) -> const_prop_binop bin_op (const_prop_expr expr1) (const_prop_expr expr2)
   | Eunop(u,e) -> const_prop_unop u (const_prop_expr e)
   | Eint(_) -> e
   | Evar(s) -> e

let constant_propagation_instr (i: cfg_node) : cfg_node =
    match i with
    | Cassign(s,e,i) -> Cassign(s,const_prop_expr e,i)
    | Creturn(e) -> Creturn(const_prop_expr e)
    | Cprint(e,i) -> Cprint(const_prop_expr e,i)
    | Ccmp(e,i1,i2) -> Ccmp(const_prop_expr e,i1,i2)
    | Cnop(_) -> i

let constant_propagation_fun ({ cfgfunargs; cfgfunbody; cfgentry } as f: cfg_fun) =
  let ht = Hashtbl.map (fun n m ->
      constant_propagation_instr m
    ) cfgfunbody in
  { f with cfgfunbody = ht}

let constant_propagation_gdef = function
    Gfun f ->
    Gfun (constant_propagation_fun f)

let constant_propagation p =
  if !Options.no_cfg_constprop
  then p
  else assoc_map constant_propagation_gdef p

let pass_constant_propagation p =
  let cfg = constant_propagation p in
  record_compile_result "Constprop";
  dump (!cfg_dump >*> fun s -> s ^ "1") dump_cfg_prog cfg
    (call_dot "cfg-after-cstprop" "CFG after Constant Propagation");
  OK cfg
